# README #

This is an example Tetris implementation that somewhat complies to the ["official" Tetris Guideline](http://tetris.wikia.com/wiki/Tetris_Guideline).

It was created as a demo project to test the capabilities and interoperability between scala, akka and scalafx.

Download & run [the jar](https://bitbucket.org/mucaho/scalatrix/downloads/Scalatrix.jar) or fork the project and run `src/scalatrix/Main.scala`.