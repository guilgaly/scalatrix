package actor

import akka.actor.Actor
import scalatrix.Util.ScalatrixMessage._
import scalatrix.GameState

/**
 * The model. Updates the [[scalatrix.GameState game state]] (currently falling block and game board).
 */
class Model extends Actor {

  import context._

  def updateState(state: GameState): Receive = {
    case t @ TransformBlock(transform) => {
      // update the gamestate
      val newState = state.transform(transform)

      become(updateState(newState))

      newState match {
        // falling block position changed, send new block position
        case GameState(Some(block), _) => sender ! UpdateBlock(block)
        // falling block was merged with board, send new board
        case GameState(None, _) => sender ! UpdateBoard(newState.board)
      }
    }
  }

  override def receive: Receive = updateState(GameState())

}