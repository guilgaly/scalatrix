package scalatrix


import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle
import scalafx.scene.layout.GridPane
import Util._


/**
 * Singleton that manages the actual JavaFx display elements.
 */
object GUI extends JFXApp {
  final val TILE_SIZE = 32

  var rects: Map[Tile[Int], Rectangle] = Map()

  stage = new JFXApp.PrimaryStage {
    val pane = new GridPane()
    val tileSize = add(-1, min(pane.width / COLS, pane.height / (ROWS - 2)))

    var rect: Rectangle = null
    for (row <- 2 until ROWS; col <- 0 until COLS) {
      rect = Rectangle(0d, 0d)
      rect.fill = Color.GRAY
      rect.width <== tileSize
      rect.height <== tileSize
      rects += Tile(row, col) -> rect
      pane.add(rect, col, row)
    }



    title = "Scalatrix"
    width = 200
    height = 400
    scene = new Scene {
      root = pane
    }
  }
}