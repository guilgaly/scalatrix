package scalatrix


object Util {
  final val ROWS = 22
  final val COLS = 10

  /**
   * The transforms enumeration.
   */
  object Transform extends Enumeration {
    final type Transform = Value
    val RotateLeft = Value("RotateLeft")
    val RotateRight = Value("RotateRight")
    val MoveUp = Value("MoveUp")
    val MoveDown = Value("MoveDown")
    val MoveLeft = Value("MoveLeft")
    val MoveRight = Value("MoveRight")
  }


  /**
   * The messages that are passed between the [[actor.Controller controller]],
   * [[actor.Model]] model and [[actor.View]] view.
   */
  object ScalatrixMessage {
    import scalatrix.Util.Transform._

    sealed trait ScalatrixMessage
    // Controller -> Controller; game loop tick
    case object Tick extends ScalatrixMessage
    // (View ->) Controller -> Model; instruction to transform the currently falling block
    case class TransformBlock(transform: Transform) extends ScalatrixMessage
    // Model -> Controller -> View
    //        update the display of currently falling block
    case class UpdateBlock(block: Block) extends ScalatrixMessage
    //        update the display of current game board
    case class UpdateBoard(block: Block) extends ScalatrixMessage
  }

  object ScalatrixState {
    sealed trait State
    case object Active extends State
    case object Uninitialized extends State
  }

  final class TileAssoc[T : Numeric](val __leftOfArrow: T) {
    def neg(arg: T): T = implicitly[Numeric[T]].negate(arg)

    @inline def <>(y: T): Tile[T] = Tile(__leftOfArrow, y)
    @inline def <>-(y: T): Tile[T] = <>(neg(y))
  }
  @inline implicit def any2TileAssoc[T : Numeric](x: T): TileAssoc[T] = new TileAssoc(x)


  implicit def float2int(x: Float): Int = x.toInt
}